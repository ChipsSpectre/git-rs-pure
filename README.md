# Git-rs pure

Educational implementation of μgit, a simplified version of the popular git software versioning system.

Based on the blog [Git Internals](https://www.leshenko.net/p/ugit/#). 
