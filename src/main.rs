mod base;
mod data;
mod utils;

use std::cmp::min;

use base::{get_oid, Commit};
use data::HEAD;

fn init() {
    base::init();
}

fn hash_object(file_path: &str, obj_type: &str) {
    let buffer = utils::read_file(file_path);
    println!("{}", data::hash_object(&buffer, obj_type));
}

fn cat_file(oid: &str) {
    let buffer: Vec<u8> = data::get_object(&base::get_oid(oid), None);
    let content = buffer.into_iter().map(|x| x as char).collect::<String>();
    println!("{}", content);
}

fn write_tree() {
    let oid = base::write_tree(".");
    println!("{}", oid);
}

fn read_tree(tree_oid: &str) {
    let actual_oid = base::get_oid(tree_oid);
    base::read_tree(&actual_oid);
}

fn commit(commit_message: &str) {
    if !commit_message.starts_with("-m") {
        panic!("Invalid format for commit message!");
    }
    let actual_message: String = commit_message.chars().skip(2).collect();
    println!("{}", base::commit(&actual_message));
}

fn log(oid: Option<String>) {
    let mut curr_oid = oid.clone();
    if oid == None {
        curr_oid = Some(data::get_ref(HEAD, true).unwrap().value);
        if curr_oid == None {
            return; // nothing to log
        }
    }
    curr_oid = Some(base::get_oid(&curr_oid.unwrap()));
    base::log(curr_oid);
}

fn checkout(name: &str) {
    base::checkout(name);
}

fn tag(name: &str, oid: Option<String>) {
    let mut actual_oid = oid.unwrap_or(
        data::get_ref(HEAD, true)
            .expect("No head exists and no oid given!")
            .value,
    );
    actual_oid = base::get_oid(&actual_oid);
    base::tag(name, &actual_oid);
}

fn k() {
    let mut dot: String = "digraph commits {\n".to_owned();
    let mut oids = Vec::new();
    for reference in data::get_refs("refs/", false) {
        dot += &("\"".to_owned() + &reference.0 + "\"[shape=note]\n");
        dot += &("\"".to_owned() + &reference.0 + "\"->\"" + &reference.1.value + "\"\n");
        if !reference.1.symbolic {
            oids.push(reference.1);
        }
    }

    for oid in base::get_commits_and_parents(oids) {
        let oid_len = oid.value.len();
        let oid_label = oid.value.chars().take(min(oid_len, 10)).collect::<String>();
        dot += &("\"".to_owned()
            + &oid.value
            + "\"[shape=box style=filled label=\""
            + &oid_label
            + "\"]\n");

        let commit = base::get_commit(&oid.value);
        let parent_str = commit.parent.unwrap_or("".to_owned());
        if parent_str.len() > 0 {
            dot += &("\"".to_owned() + &oid.value + "\"->\"" + &parent_str + "\"\n");
        }
        // println!("{}. Parent={}", oid, parent_str);
    }
    dot += "}\n";
    println!("{}", dot); // print to stdin. This way, output can be plotted using graphviz.
}

fn branch(name: Option<String>, start_point: Option<String>) {
    match name {
        Some(branch_name) => {
            let start = start_point.unwrap_or("@".to_owned());
            let start_point_oid = get_oid(&start);

            base::create_branch(&branch_name, start_point_oid.clone());

            println!(
                "Created branch {} at {}",
                branch_name,
                start_point_oid.chars().take(10).collect::<String>()
            );
        }
        None => {
            let current = base::get_branch_name().unwrap();
            for branch in base::get_branch_names() {
                let prefix = if current == branch { "*" } else { " " };
                println!("{} {}", prefix, branch);
            }
        }
    }
}

fn status() {
    let head_oid = base::get_oid("@");
    let branch_name = base::get_branch_name();
    match branch_name {
        Some(name) => {
            println!("On branch {}", name);
        }
        None => {
            println!(
                "HEAD detached at {}",
                head_oid.chars().take(10).collect::<String>()
            );
        }
    }
}

fn reset(oid: &str) {
    base::reset(oid);
}

fn show(oid: &str) {
    let actual_oid = base::get_oid(oid);
    let commit = base::get_commit(&actual_oid);
    base::print_commit(&actual_oid, &commit);
}

fn main() {
    let command = std::env::args().nth(1).expect("no command given!");
    match command.as_ref() {
        "init" => {
            init();
        }
        "hash-object" => {
            let file_path = std::env::args().nth(2).expect("no file path given!");
            hash_object(file_path.as_ref(), "blob");
        }
        "cat-file" => {
            let oid = std::env::args().nth(2).expect("no oid given!");
            cat_file(oid.as_ref());
        }
        "write-tree" => {
            write_tree();
        }
        "read-tree" => {
            let tree_oid = std::env::args().nth(2).expect("no tree oid given!");
            read_tree(tree_oid.as_ref());
        }
        "commit" => {
            let msg = std::env::args().nth(2).expect("no commit message given!");
            commit(msg.as_ref());
        }
        "log" => {
            let msg = std::env::args().nth(2);
            log(msg);
        }
        "checkout" => {
            let name = std::env::args().nth(2).expect("no name given!");
            checkout(&name);
        }
        "tag" => {
            let tag_name = std::env::args().nth(2).expect("no tag name given!");
            let oid = std::env::args().nth(3);
            tag(&tag_name, oid);
        }
        "k" => {
            k();
        }
        "branch" => {
            let branch_name = std::env::args().nth(2);
            let branch_start_point = std::env::args().nth(3);
            branch(branch_name, branch_start_point);
        }
        "status" => {
            status();
        }
        "reset" => {
            let reset_oid = std::env::args().nth(2).expect("no oid given for reset!");
            reset(&reset_oid);
        }
        "show" => {
            let oid = std::env::args().nth(2).unwrap_or("@".to_owned());
            show(&oid);
        }

        _ => {}
    }
}
