#![allow(non_snake_case)]

use crate::utils;
use sha1::{Digest, Sha1};
use std::fs;
use std::path::Path;

pub const GIT_DIR: &str = ".git-rs";
pub const HEAD: &str = "HEAD";

pub fn init() {
    fs::create_dir_all(GIT_DIR).expect("Could not initialize repository!");
    fs::create_dir_all(GIT_DIR.to_owned() + "/objects")
        .expect("Could not initialize objects directory!");
    fs::create_dir_all(GIT_DIR.to_owned() + "/refs/tags")
        .expect("Could not initialize tags directory!");
    fs::create_dir_all(GIT_DIR.to_owned() + "/refs/heads")
        .expect("Could not initialize branches directory!");
}

fn hex_char(value: u8) -> char {
    if value < 10 {
        return (value + 48) as char; // number
    }
    return (value - 10 + 97) as char; // hex character
}

pub fn hash_object(data: &Vec<u8>, obj_type: &str) -> String {
    let mut obj: Vec<u8> = Vec::new(); // create obj with type information
    obj.append(&mut obj_type.to_owned().into_bytes());
    obj.append(&mut "\0".to_owned().into_bytes());
    obj.append(&mut data.clone());

    let mut hasher = Sha1::new();
    hasher.update(&obj);
    let oid_vector = hasher.finalize(); // git object id
                                        // create hex digest
    let mut oid = String::new();
    for byte in oid_vector {
        oid.push(hex_char(byte / 16));
        oid.push(hex_char(byte % 16));
    }
    // write the data to the right file
    std::fs::write(GIT_DIR.to_owned() + "/objects/" + &oid, obj).expect("Failed to hash object!");
    return oid;
}

pub fn get_object(oid: &str, expected_type: Option<String>) -> Vec<u8> {
    let path = GIT_DIR.to_owned() + "/objects/" + oid;
    let buffer = utils::read_file(&path);

    let mut null_idx = 0; // index of null character
    for c in &buffer {
        if *c == 0 {
            break;
        }
        null_idx += 1;
    }
    match expected_type {
        Some(expected) => {
            // check expected type if needed.
            let obj_type = buffer
                .iter()
                .take(null_idx)
                .map(|x| *x as char)
                .collect::<String>();
            if expected != obj_type {
                panic!("Expected and actual type do not match!");
            }
        }
        None => {}
    }
    return buffer.into_iter().skip(null_idx + 1).collect::<Vec<u8>>();
}

#[derive(Clone, PartialEq)]
pub struct RefValue {
    pub symbolic: bool,
    pub value: String,
}

pub fn get_ref(reference: &str, deref: bool) -> Option<RefValue> {
    let path = GIT_DIR.to_owned() + "/" + reference;
    if !Path::new(&path).exists() {
        return None;
    }
    return Some(get_ref_internal(reference, deref).1);
}

pub fn update_ref(reference: &str, data: &RefValue, deref: bool) {
    let act_reference = get_ref_internal(reference, deref).0;
    let ref_path = GIT_DIR.to_owned() + "/" + &act_reference;

    let mut value_to_write = data.value.clone();
    if data.symbolic {
        value_to_write = "ref:".to_owned() + &value_to_write;
    }
    std::fs::write(ref_path, value_to_write.into_bytes()).expect("Could not write reference.");
}

pub fn get_ref_internal(reference: &str, deref: bool) -> (String, RefValue) {
    let path = GIT_DIR.to_owned() + "/" + reference;
    let mut oid_bytes = Vec::new();
    if Path::new(&path).exists() {
        oid_bytes = utils::read_file(&path);
    } 
    let mut oid = oid_bytes.iter().map(|x| *x as char).collect::<String>();
    let symbolic = oid.len() > 0 && oid.starts_with("ref:");
    if symbolic {
        oid = oid.chars().skip("ref:".len()).collect::<String>();
        if deref {
            return get_ref_internal(&oid, deref);
        }
    }
    return (
        reference.to_owned(),
        RefValue {
            symbolic: symbolic,
            value: oid,
        },
    );
}

pub fn get_refs(prefix: &str, deref: bool) -> Vec<(String, RefValue)> {
    // gets all filenames of references.
    let mut reference_file_paths = vec![HEAD.to_owned()];
    let mut queue = vec![prefix.to_owned()];
    while !queue.is_empty() {
        let curr = queue.last().unwrap().clone();
        queue.pop();
        let entries_path = GIT_DIR.to_owned() + "/" + &curr;
        let entries = std::fs::read_dir(entries_path).expect("Could not read dir!");
        for entry in entries {
            let actual_entry = entry.expect("Could not access directory entry!");
            if actual_entry.path().is_dir() {
                // skip directories
                let dir_path: String = actual_entry
                    .path()
                    .into_os_string()
                    .into_string()
                    .expect("Could not convert string!")
                    .chars()
                    .skip(GIT_DIR.len())
                    .collect();
                queue.push(dir_path);
            } else {
                let name: String = actual_entry
                    .path()
                    .into_os_string()
                    .into_string()
                    .expect("Could not convert string!")
                    .chars()
                    .skip(GIT_DIR.len() + 1)
                    .collect();
                reference_file_paths.push(name);
            }
        }
    }
    let return_value = reference_file_paths
        .into_iter()
        .map(|x| (x.clone(), get_ref(&x, deref).unwrap()))
        .collect::<Vec<(String, RefValue)>>();
    return return_value;
}
