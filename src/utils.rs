use std::fs::File;
use std::io::BufReader;
use std::io::Read;

pub fn read_file(file_path: &str) -> Vec<u8> {
    let mut buffer = Vec::new();
    let file = File::open(file_path).expect("Could not open file!");
    let mut reader = BufReader::new(file);
    reader
        .read_to_end(&mut buffer)
        .expect("Reading the file failed!");
    return buffer;
}
