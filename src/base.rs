use crate::{
    data::{self, get_ref, get_refs, hash_object, update_ref, RefValue, HEAD},
    utils,
};

use std::{
    collections::{HashMap, HashSet, VecDeque},
    fs,
};

fn is_ignored(path: &str) -> bool {
    if path.starts_with("./.git-rs") {
        return true;
    }
    if path.starts_with("./.git") {
        return true;
    }
    if path.starts_with("./target") {
        return true;
    }
    return false;
}

pub fn write_tree(directory: &str) -> String {
    // write all files in the current directory.
    let paths = fs::read_dir(directory).expect("Could not read directory paths.");
    let mut entries = Vec::new();
    for path in paths {
        let path_entry = path.unwrap();
        if is_ignored(&path_entry.path().into_os_string().into_string().unwrap()) {
            continue;
        }
        let entry_name = path_entry.file_name();
        if path_entry.path().is_dir() {
            let obj_type = "tree";
            let oid = write_tree(&path_entry.path().into_os_string().into_string().unwrap());
            entries.push((entry_name, oid, obj_type));
        } else {
            // write file
            let obj_type = "blob";
            let buffer =
                utils::read_file(&path_entry.path().into_os_string().into_string().unwrap());
            let oid = hash_object(&buffer, obj_type);
            entries.push((entry_name, oid, obj_type));
        }
    }
    let tree = entries
        .iter()
        .map(|x| x.0.clone().into_string().unwrap().to_owned() + " " + &x.1 + " " + x.2 + "\n")
        .collect::<String>();
    return data::hash_object(&tree.into_bytes(), "tree");
}

fn read_tree_rec(curr_dir_oid: &str, base_path: String) {
    let tree = data::get_object(curr_dir_oid, Some("tree".to_owned()));
    let tree_entries = String::from_utf8(tree).unwrap();
    let num_entries = tree_entries.split("\n").count();
    for tree_entry in tree_entries.split("\n").take(num_entries - 1) {
        let obj_name = tree_entry.split(" ").nth(0).unwrap();
        let obj_oid = tree_entry.split(" ").nth(1).unwrap();
        let obj_type = tree_entry.split(" ").nth(2).unwrap();
        match obj_type {
            "tree" => {
                let new_base_path = base_path.clone() + "/" + obj_name;
                read_tree_rec(obj_oid, new_base_path);
            }
            "blob" => {
                let content = data::get_object(obj_oid, Some("blob".to_owned()));
                let content_path = base_path.clone() + "/" + obj_name;
                fs::write(&content_path, content)
                    .expect("Could not read file from object database!");
            }
            _ => {}
        }
    }
}

fn empty_current_directory(directory: &str) {
    let paths = fs::read_dir(directory).expect("Could not read directory paths.");

    for path in paths {
        let path_entry = path.unwrap();
        let path_string = path_entry.path().into_os_string().into_string().unwrap();
        if is_ignored(&path_string) {
            continue;
        }
        if path_entry.path().is_dir() {
            empty_current_directory(&path_string);
        } else {
            fs::remove_file(path_entry.path()).expect("Could not delete file!");
        }
    }
}

pub fn read_tree(directory_oid: &str) {
    empty_current_directory(".");
    read_tree_rec(directory_oid, ".".to_owned());
}

pub fn commit(commit_message: &str) -> String {
    let tree_oid = write_tree(".");
    let mut commit_content = "tree ".to_owned();
    commit_content += &tree_oid;
    commit_content += "\nparent ";
    let parent = data::get_ref(HEAD, true);
    match parent {
        Some(parent_oid) => {
            commit_content += &parent_oid.value;
        }
        None => {}
    }
    commit_content += "\n";
    commit_content += commit_message;
    commit_content += "\n";

    let commit_oid = data::hash_object(&commit_content.into_bytes(), "commit");

    data::update_ref(
        HEAD,
        &RefValue {
            symbolic: false,
            value: commit_oid.clone(),
        },
        true,
    );
    return commit_oid;
}

pub struct Commit {
    tree: String,
    pub parent: Option<String>,
    message: String,
}

pub fn get_commit(oid: &str) -> Commit {
    let mut actual_oid = oid.to_owned();
    if oid.starts_with("ref:") {
        actual_oid = actual_oid.chars().skip("ref:".len()).collect();
    }
    let commit_content: String = data::get_object(&actual_oid, Some("commit".to_owned()))
        .into_iter()
        .map(|x| x as char)
        .collect();
    let content_lines = commit_content.split("\n").collect::<Vec<&str>>();

    let tree = content_lines[0].split(" ").collect::<Vec<&str>>()[1].to_owned();
    let mut parent = None;
    if content_lines[1].starts_with("parent") {
        let parent_oid = content_lines[1].split(" ").collect::<Vec<&str>>()[1].to_owned();
        if parent_oid.len() > 0 {
            parent = Some(parent_oid);
        }
    }
    return Commit {
        tree: tree,
        parent: parent,
        message: content_lines[2].to_owned(),
    };
}

pub fn print_commit(oid: &str, commit: &Commit) {
    println!("Commit {}", oid);
    println!("Parent {}", commit.parent.clone().unwrap_or_default());
    println!("");
    println!("{}", commit.message);
    println!("");
    println!("");
}

pub fn log(start_oid: Option<String>) {
    let references = get_refs("refs/", true);
    let mut ref_map: HashMap<String, Vec<String>> = HashMap::new();
    for reference in references {
        if ref_map.contains_key(&reference.1.value) {
            ref_map
                .get_mut(&reference.1.value)
                .map(|val| val.push(reference.0));
        } else {
            ref_map.insert(reference.1.value, vec![reference.0]);
        }
    }

    match start_oid {
        None => {}
        Some(oid) => {
            let all_oids = get_commits_and_parents(vec![RefValue {
                symbolic: false,
                value: oid,
            }]);
            for curr_oid in all_oids {
                let curr_commit = get_commit(&curr_oid.value);
                print_commit(&curr_oid.value, &curr_commit);
            }
        }
    }
}

fn is_branch(name: &str) -> bool {
    match get_ref(&("refs/heads/".to_owned() + name), true) {
        Some(_) => {
            return true;
        }
        None => {
            return false;
        }
    }
}

pub fn checkout(name: &str) {
    let oid = get_oid(name);

    let commit_content = get_commit(&oid);
    let tree_oid = commit_content.tree;
    read_tree(&tree_oid);

    let mut curr_head = RefValue {
        symbolic: false,
        value: oid,
    };
    if is_branch(name) {
        curr_head = RefValue {
            symbolic: false,
            value: "ref:refs/heads/".to_owned() + name,
        };
    }

    data::update_ref(HEAD, &curr_head, false);
}

pub fn tag(name: &str, oid: &str) {
    data::update_ref(
        // baz
        &("refs/tags/".to_owned() + name),
        &RefValue {
            symbolic: false,
            value: oid.to_string(),
        },
        true,
    );
}

pub fn get_oid(data: &str) -> String {
    if data == "@" {
        return data::get_ref(HEAD, true).expect("No head found!").value;
    }
    let refs_to_try = vec![
        data.to_owned(),
        "refs/".to_owned() + data,
        "refs/tags/".to_owned() + data,
        "refs/heads/".to_owned() + data,
    ];
    for reference in refs_to_try {
        let curr_ref = data::get_ref(&reference, false);
        match curr_ref {
            Some(oid) => {
                return oid.value;
            }
            None => {}
        }
    }
    return data.to_owned();
}

pub fn get_commits_and_parents(oids: Vec<RefValue>) -> Vec<RefValue> {
    let mut result = Vec::new();
    let mut visited: HashSet<String> = HashSet::new();

    let mut curr_oids = oids.clone().into_iter().collect::<VecDeque<RefValue>>();
    for oid in oids {
        curr_oids.push_back(oid.clone());
    }

    while !curr_oids.is_empty() {
        let entry = curr_oids[0].clone();
        curr_oids.pop_front();
        if visited.contains(&entry.value) {
            continue;
        }
        visited.insert(entry.clone().value);
        result.push(entry.clone());

        let commit_info = get_commit(&entry.value);
        if commit_info.parent != None {
            curr_oids.push_back(RefValue {
                symbolic: false,
                value: commit_info.parent.unwrap(),
            });
        }
    }

    return result;
}

pub fn create_branch(name: &str, oid: String) {
    update_ref(
        &("refs/heads/".to_owned() + name),
        &RefValue {
            symbolic: false,
            value: oid.to_string(),
        },
        true,
    );
}

pub fn init() {
    data::init();
    data::update_ref(
        HEAD,
        &RefValue {
            symbolic: true,
            value: "refs/heads/master".to_owned(),
        },
        true,
    );
}

pub fn get_branch_name() -> Option<String> {
    let head_ref = get_ref(HEAD, false);
    let is_symbolic = head_ref.clone().unwrap().symbolic;
    if !is_symbolic {
        return None;
    }
    let head_name = head_ref.unwrap().value;
    if !head_name.starts_with("refs/heads/") {
        panic!("Invalid symbolic branch name location found!");
    }
    return Some(head_name.chars().skip("refs/heads/".len()).collect());
}

pub fn get_branch_names() -> Vec<String> {
    let head_refs = get_refs("refs/heads/", false);
    let mut result: Vec<String> = Vec::new();
    for head_ref in head_refs {
        result.push(head_ref.0.chars().skip("refs/heads/".len()).collect());
    }
    return result;
}

pub fn reset(oid: &str) {
    data::update_ref(HEAD, &RefValue { symbolic: false, value: oid.to_owned() }, true);
}
